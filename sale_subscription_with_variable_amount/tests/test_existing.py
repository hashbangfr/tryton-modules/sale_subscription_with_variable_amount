import pytest  # noqa
from datetime import date # noqa
from decimal import Decimal # noqa
from hb.tryton.devtools.helper import accounts # noqa
from hb.tryton.devtools.tests.testing import (
    get_company, create_chart, create_user, set_user,
    create_recurrence_rule_set, create_product_category,
    create_subscription_service, create_subscription,
    wizard_subscription_create_consumption_lines,
    wizard_subscription_create_invoices_from_consumption,
)


class TestExisting:

    def test_defaut_hook(self, rollbacked_transaction, pool):
        ServiceType = pool.get('sale.subscription.service.type')
        st = ServiceType(name='test')
        st.save()
        assert st.consumption_hook == 'fixed'
        assert st.invoice_hook == 'fixed'

    def test_existing(self, rollbacked_transaction, pool):
        """Test the existing use case

        This unit test is a back port of one of the scenario
        from sale_subscription the goal is to verify the
        initial case
        """
        Party = pool.get('party.party')
        Address = pool.get('party.address')
        InvoiceLine = pool.get('account.invoice.line')

        company = get_company()
        create_chart(company=company)
        user = create_user(company=company, group_names=['Sales'])
        with set_user(login=user.login):
            monthly = create_recurrence_rule_set()
            daily = create_recurrence_rule_set(freq='daily')
            product_category = create_product_category(company)
            customer = Party(name='Customer', addresses=[Address(invoice=True)])
            customer.save()

            service = create_subscription_service(
                account_category=product_category, recurrence=daily)
            assert service.service_type  # default

            subscription = create_subscription(
                customer=customer, recurrence=monthly, service=service)
            assert subscription.state == 'running'

            wizard_subscription_create_consumption_lines()
            assert len(pool.get('sale.subscription.line.consumption').search(
                [])) == 31

            wizard_subscription_create_invoices_from_consumption()

            line = InvoiceLine.search(
                [('origin', '=', subscription.lines[0])])[0]
            assert line.quantity == 310.0
            assert line.unit_price == Decimal('10.0000')
